<?php
namespace MagentoConfigEAV\ModuleHello\Model\Attribute\Backend;

class CustomAttribute extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * Validate
     * @param \Magento\Catalog\Model\Product $object
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return bool
     */
    public function validate($object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        if ($value > 15) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Value greater than 15')
            );
        }
        return true;
    }

    public function beforeSave($object)
    {
        $changedValue = 5;
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        if($value < 7)
        {
            $value = $changedValue;
        }
        try {
            $this->_configValueFactory->create()->setValue($value)->save();
        } catch (\Exception $e) {
            throw new \Exception(__($value));
        }
    }

    public function afterSave($object)
    {
        return parent::afterSave($object);
    }

}
