<?php

namespace MagentoConfigEAV\ModuleHello\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;

class UpgradeData implements UpgradeDataInterface
{
    private $_eavSetupFactory;
    private $eavConfig;
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig
    )
    {
        $this->eavConfig=$eavConfig;
        $this->_eavSetupFactory = $eavSetupFactory;
    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.9.833', '<')) {
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'new_product_attribute',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Test New Upgraded Product Attribute',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

//            $eavSetup->addAttribute(
//                \Magento\Catalog\Model\Product::ENTITY,
//                'test',
//                [
//                    'type' => 'text',
//                    'backend' => '',
//                    'frontend' => '',
//                    'label' => 'Test',
//                    'input' => 'text',
//                    'class' => '',
//                    'source' => '',
//                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
//                    'visible' => true,
//                    'required' => true,
//                    'user_defined' => false,
//                    'default' => '',
//                    'searchable' => false,
//                    'filterable' => false,
//                    'comparable' => false,
//                    'visible_on_front' => false,
//                    'used_in_product_listing' => true,
//                    'unique' => false,
//                    'apply_to' => ''
//                ]
//            );




            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'option_custom_attribute',
                [
                    'type' => 'int',
                    'backend' => 'MagentoConfigEAV\ModuleHello\Model\Attribute\Backend\CustomAttribute',
                    'frontend' => '',
                    'label' => 'Options from 1-20',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'MagentoConfigEAV\ModuleHello\Model\Attribute\Source\CustomAttribute',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => true,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

            $eavSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                'new_customer_attribute',
                [
                    'type' => 'varchar',
                    'label' => 'Demo Attribute',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => true,
                    'position' => 999,
                    'system' => 0,
                ]
            );
            $sampleAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'new_customer_attribute');
            $sampleAttribute->setData(
                'used_in_forms',
                ['adminhtml_customer']
            );
            $sampleAttribute->save();
        }
    }
}
